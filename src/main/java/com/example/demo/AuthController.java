package com.example.demo;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController {

    AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    public ResponseEntity<String> login(String username, String password) {
        try {
            return ResponseEntity.ok(authService.login(username, password));
        } catch (AuthFailedException e) {
            return ResponseEntity
                    .status(401)
                    .build();
        }
    }
}
