package com.example.demo;

import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthService {
    TokenRepository tokenRepository;
    UserRepository userRepository;
    PasswordVerifier passwordVerifier;

    public AuthService(TokenRepository tokenRepository, UserRepository userRepository, PasswordVerifier passwordVerifier) {
        this.tokenRepository = tokenRepository;
        this.userRepository = userRepository;
        this.passwordVerifier = passwordVerifier;
    }

    public String login(String username, String password) throws AuthFailedException {
        return userRepository.findUserByUsername(username)
                .filter(user -> passwordVerifier.testPassword(password, user.getEncryptedPassword()))
                .map(user -> tokenRepository.createToken())
                .orElseThrow(AuthFailedException::new);
    }
}
