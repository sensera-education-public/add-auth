package com.example.demo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SpringBootTest
public class AuthControllerTest {

    @Autowired AuthController authController;

    @MockBean AuthService authService;

    String testToken;

    @BeforeEach
    void setUp() {
        testToken = UUID.randomUUID().toString();
    }

    @Test
    void test_login_success() throws AuthFailedException {
        // Given
        when(authService.login(anyString(), anyString())).thenReturn(testToken);

        // When
        ResponseEntity<String> token = authController.login("Arne","Losen");

        // Then
        assertEquals(200, token.getStatusCodeValue());
        assertEquals(testToken, token.getBody());
    }

    @Test
    void test_login_failed() throws AuthFailedException {
        // Given
        when(authService.login(anyString(), anyString())).thenThrow(new AuthFailedException());

        // When
        ResponseEntity<String> token = authController.login("Arne","Losen");

        // Then
        assertEquals(401, token.getStatusCodeValue());
        assertNull(token.getBody());
    }
}
