package com.example.demo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@SpringBootTest
class AuthServiceTest {

    @Autowired AuthService authService;

    @MockBean TokenRepository tokenRepository;
    @MockBean UserRepository userRepository;
    @MockBean PasswordVerifier passwordVerifier;

    String testToken;

    @BeforeEach
    void setUp() {
        testToken = UUID.randomUUID().toString();
    }

    @Test
    void test_login_success() throws AuthFailedException {
        // Given
        User user = User.create("",new byte[0]);
        when(userRepository.findUserByUsername(eq("Arne"))).thenReturn(Optional.of(user));
        when(tokenRepository.createToken()).thenReturn(testToken);
        when(passwordVerifier.testPassword(eq("Losen"), any())).thenReturn(true);

        // When
        String token = authService.login("Arne", "Losen");

        // Then
        assertEquals(testToken, token);
    }

    @Test
    void test_login_failed_because_wrong_username() throws AuthFailedException {
        // Given
        when(userRepository.findUserByUsername(eq("Arne"))).thenReturn(Optional.empty());

        // When
        AuthFailedException authFailedException = assertThrows(AuthFailedException.class, () -> authService.login("Arne", "Losen"));

        // Then
        assertNotNull(authFailedException);
    }

    @Test
    void test_login_failed_because_wrong_password() throws AuthFailedException {
        // Given
        User user = User.create("",new byte[0]);
        when(userRepository.findUserByUsername(eq("Arne"))).thenReturn(Optional.of(user));
        when(passwordVerifier.testPassword(eq("Losen"), any())).thenReturn(false);

        // When
        AuthFailedException authFailedException = assertThrows(AuthFailedException.class, () -> authService.login("Arne", "Losen"));

        // Then
        assertNotNull(authFailedException);
    }
}
